= Alpine Linux with Apache `httpd` for Subversion and Trac (`alpine-httpd-subversion_trac`)
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This project provides a Docker container image.
Using a container based on this image, you can serve a https://subversion.apache.org/[Subversion] VCS repo using https://httpd.apache.org/[Apache httpd web server], and a https://trac.edgewall.org/[Trac] instance in another container.

== Dependencies

[options="header",cols=",,,m"]
|===
| Conditions | Type | Name (URL) | Version constraint

| by necessity
| software
| https://www.docker.com/[Docker Compose]
| ==1.8.0

| by necessity
| software
| https://www.docker.com/[Docker Engine]
| ==1.12.1

| by necessity
| image
| https://gitlab.com/CLARIN-ERIC/alpine-httpd[`alpine-httpd`]
| ==1.0.0

| for Docker Engine interaction
| software
| https://www.sudo.ws/[Sudo]
| >=1.8

| for releases
| platform
| https://about.gitlab.com[GitLab CI]
| ==8.10.4

|===

== To use

The best way to discover how to use the image is by reading the link:docker-compose.yml[`docker-compose.yml`] (Docker Compose) file.

The images are designed around restoring data from backups/dumps.
As declared in this Docker Compose file, these files should be available from `/srv/alpine-httpd-subversion_trac/`.

In addition, there is a link:deployment.yml[`deployment.yml`] Docker Compose file.
This example file contains deployment-specific information, i.e. secrets and networking configuration.
It should be configured for production, and that actual file should not be added to this repo.

=== SVN

==== To make a dump

[source,sh]
----
svnadmin dump "$SVN_REPO_ROOT_DIR" > SVN_dump
----

=== Trac

==== To make a dump

[source,sh]
----
trac-admin "$TRAC_ROOT_DIR" hotcopy Trac_dump
tar -c -p -z -f Trac_dump.tar.gz Trac_dump/
rm -fr -- Trac_dump/
----

Backup archive layout:
[source,sh]
----
./db/postgres-db-backup.sql.gz
...
----

== To build and test

The build, test and release pipeline is specified in the the link:.gitlab-ci.yml[GitLab CI] config file.
This pipeline or its stages can be run by GitLab.com CI as well as by a self-hosted GitLab instance.
This is elaborated in the https://about.gitlab.com/gitlab-ci/[GitLab CI docs].
In addition, the build and test stages can be run be run locally within a bare Docker container, without a GitLab CI pipeline.

NOTE: Please issue all of the following shell statements from within the root directory of this repository.

=== Without a GitLab CI pipeline

[source,sh]
----
sudo systemctl start docker.service
Docker_engine_ver="$(sudo docker version --format '{{.Server.Version}}')"
sudo docker run --volume='/var/run/docker.sock:/var/run/docker.sock' \
    --rm --volume="$PWD":"$PWD" --workdir="$PWD" -it \
    docker:"$Docker_engine_ver" sh
----

Within the container you now entered, to build and test without a time-out value:

[source,sh]
----
sh -x ./build.sh && sh -x ./test.sh
----

==== Create backups

Making a http://svnbook.red-bean.com/en/1.7/svn.ref.svnadmin.c.dump.html[svn dump]:

[source,sh]
----
svnadmin dump /srv/subversion/svn.clarin.eu/ | gzip > /home/wilelb/svn.dump.gz
----

Making a https://trac.edgewall.org/wiki/TracBackup[trac dump]:
[source,sh]
----

----


[source,sh]
----
docker run --rm --name svn_test \
    --tmpfs "/run/apache2/:rw,gid=101,mode=700,noatime,nodev,noexec,uid=100" \
    --tmpfs "/tmp/:rw,noatime,nodev,noexec,mode=1777" \
    -e '_SERVERNAME=svn.localdomain' \
    -e 'AUTH_LDAP_BIND_DN=uid=read_only,ou=operators,dc=localdomain' \
    -e 'AUTH_LDAP_BIND_PASSWORD=testtesttest123!' \
    -e 'AUTH_LDAP_REQUIRE=ldap-group cn=developer,ou=groups,dc=localdomain' \
    -e 'AUTH_LDAP_URL=ldap://ldap.localdomain/ou=members,dc=localdomain?mail?sub?(objectClass=person)' \
    alpine-httpd-subversion_trac:2.0.1-2-g1292656 --init --svn
----


==== Trac plugins

The following list of plugins is installed in this image:

* https://trac-hacks.org/wiki/MarkdownMacro[MarkdownMacro]
* https://trac-hacks.org/wiki/IncludeMacro[IncludeMacro]
* https://trac-hacks.org/wiki/AutocompleteUsersPlugin[AutocompleteUsersPlugin]
* https://trac-hacks.org/wiki/TracWysiwygPlugin[TracWysiwygPlugin]
* https://trac-hacks.org/wiki/NumberedHeadlinesPlugin[NumberedHeadlinesPlugin]