#!/bin/bash

function backup_svn {
    DATE=$(date '+%Y%m%d_%H%M%S')
    svnadmin dump /srv/subversion > /backups/svn.dump
    tar -pczvf /backups/svn_${DATE}.dump.tar.gz -C /backups svn.dump
    rm -f /backups/svn.dump
}

function backup_trac {
    #Remove earlier datbase dumps to avoid accumulation
    rm -f /srv/trac/data/db/postgres-db-backup.sql.gz
    #Backup trac data and database
    trac-admin /srv/trac/data hotcopy /backups/work
    #Compress
    DATE=$(date '+%Y%m%d_%H%M%S')
    tar -pczf /backups/trac_${DATE}.tar.gz -C /backups/work .
    #Cleanup
    rm -rf /backups/work
}

function main {
    if [ ${MODE} == "DO_SVN" ]; then
        backup_svn
        exit 0
    elif [ ${MODE} == "DO_TRAC" ]; then
        backup_trac
        exit 0
    else
        echo "Unkown MODE=${MODE}"
        exit 1
    fi
}

main "$@"; exit