#!/bin/sh

set -e

#
# Set default values for parameters
#
_INIT_FILE="${INIT_FILE:-/var/www/dynamic_cfg/initialised}"
_SLEEP="${SLEEP:-5}"
_INIT="${INIT:-0}"
_ACCESS_MODE="${ACCESS_MODE:-private}"

function wait_for_database {
    if [ "${MODE}" = "DO_TRAC" ]; then
        while ! echo exit | nc ${DB_HOST} ${DB_PORT} </dev/null; do echo 'Waiting for database to become available'; sleep "${_SLEEP}"; done
    fi
}

function wait_for_preparer {
    while  [ ! -f  "${_INIT_FILE}" ]; do echo "Waiting for preparer to finish"; sleep "${_SLEEP}"; done
}

function initialize {
    if [ "${MODE}" = "DO_TRAC" ]; then
        while ! ls -A /srv/subversion/; do echo "Waiting for SVN repo"; sleep "${_SLEEP}"; done
    fi

    if [ ! -f  "${_INIT_FILE}" ]; then
        echo "[INIT] Initialization started"

        #Apply initialization
        echo "[INIT] Running httpd__init_entrypoint.sh"
        sh -x /usr/local/bin/alpine-httpd__init_entrypoint.sh

        if [ "${MODE}" = "DO_TRAC" ]; then
            echo "[INIT] Running trac__init_entrypoint.sh"
            echo "ACCESS_MODE=${_ACCESS_MODE}"
            if [ "${_ACCESS_MODE}" = "basic" ]; then
                sh -x /usr/local/bin/alpine-httpd-subversion_trac__init_entrypoint.sh BASIC_AUTH
            else
                sh -x /usr/local/bin/alpine-httpd-subversion_trac__init_entrypoint.sh
            fi
            echo "[INIT] Running trac__Trac_init_entrypoint.sh"
            sh /usr/local/bin/alpine-httpd-subversion_trac__Trac_init_entrypoint.sh
            cp /etc/trac.conf.template /var/www/dynamic_cfg/website_extra.conf
        elif [ "${MODE}" = "DO_SVN" ]; then
            echo "[INIT] Running trac__init_entrypoint.sh"
            sh -x /usr/local/bin/alpine-httpd-subversion_trac__init_entrypoint.sh PUBLIC_READ_ACCESS
            echo "[INIT] Running trac__Trac_init_entrypoint.sh"
            sh -x /usr/local/bin/alpine-httpd-subversion_trac__SVN_init_entrypoint.sh
            cp /etc/subversion.conf.template /var/www/dynamic_cfg/website_extra.conf
        fi

        #Ensure proper permissions are set
        chown apache:www-data /var/www -R
        chown apache:www-data /srv/trac -R

        #Test installation
        echo "[INIT] Testing configuration"
        httpd -e DEBUG -D "${MODE}" -t

        #Write initialization state to disk
        echo "[INIT] Finalizing initialization"
        touch "${_INIT_FILE}"
    else
        echo "[INIT] ${_INIT_FILE} exists, skipping initialisation"
    fi
}

function restore {
    if [ ${MODE} == "DO_SVN" ]; then
        restore_svn
    elif [ ${MODE} == "DO_TRAC" ]; then
        restore_trac
    else
        echo "Unkown MODE=${MODE}"
        exit 1
    fi
}

function restore_svn {
    mkdir -p /restore
    echo "Extracting backup"
    tar -x -C /restore -f /backups/restore
    if [ ! -f "/restore/svn.dump" ]; then
        echo "/restore/svn.dump not found!"
        exit 1
    fi
    echo "Restoring backup"
    svnadmin load /srv/subversion < /restore/svn.dump
    echo "Cleaning up"
    rm /restore/svn.dump
}

function restore_trac {
    echo "Restoring trac environment"

    if [ ! -f ~/.pgpass ]; then
        echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USER}:${PGPASSWORD}" > ~/.pgpass
        chmod 0600 ~/.pgpass
    fi

    #
    # Clear filesystem if needed
    #
    TRAC_INITIALISED=$(grep "postgres://${DB_USER}:${PGPASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}" conf/trac.ini | wc -l)
    if [ "${TRAC_INITIALISED}" == "1" ]; then
        echo "Clearing filesystem"
        trac_20180723_131644.tar.gz /srv/trac/data.bak
    fi

    #
    # Clear existing database if needed
    #
    TRAC_DB_INITIALISED=$(psql --host="${DB_HOST}" --username="${DB_USER}" --no-password --port="${DB_PORT}" -tAc "SELECT 1 FROM pg_database WHERE datname='${DB_NAME}'")
    if [ "${TRAC_DB_INITIALISED}" == "1" ]; then
        echo "Dropping database"
        psql --host="$DB_HOST" --port="$DB_PORT" --username="$DB_USER" --no-password -c "DROP DATABASE ${DB_NAME};"
    fi

    #
    # Create trac role if needed
    #
    TRAC_ROLE_EXISTS=$(psql --host="${DB_HOST}" --username="${DB_USER}" --no-password --port="${DB_PORT}" -tAc "SELECT 1 FROM pg_roles WHERE rolname='trac'")
    if [ "${TRAC_ROLE_EXISTS}" != "1" ]; then
        echo "Creating trac role"
        psql --host="$DB_HOST" --port="$DB_PORT" --username="$DB_USER" --no-password -c "CREATE ROLE trac"
    fi

    #
    # Restore filesystem
    #
    if [ -e /srv/trac/data ]; then
        if [ -e /srv/trac/data.bak ]; then
            rm -rf /srv/trac/data.bak
        fi
        mv -f /srv/trac/data /srv/trac/data.bak
    fi
    mkdir -p /srv/trac/data
    tar --directory=/srv/trac/data/ --extract --file='/backups/restore' --gunzip --same-permissions --strip-components=1

    # Cleanup plugins included in backup
    echo "Cleaning plugins included in backup"
    rm -f /srv/trac/data/plugins/*
    #Default_CC-0.1-py2.7.egg

    #
    # Restore database
    #
    #cp /backups/restore/trac_clarin_eu_backup.sql.bz2 /tmp
    DB_BACKUP_FILE="/srv/trac/data/db/postgres-db-backup.sql"
    gunzip "${DB_BACKUP_FILE}.gz"

    #Adjust database name in backup file to configured database name in $DB_NAME if needed
    DB_NAME_IN_BACKUP=$(grep -m 1 "CREATE DATABASE" "${DB_BACKUP_FILE}" | cut -d ' ' -f 3 | xargs)
    echo "DB_NAME_IN_BACKUP=${DB_NAME_IN_BACKUP}, DB_NAME=${DB_NAME}"
    if [ "${DB_NAME_IN_BACKUP}" != "${DB_NAME}" ]; then
        echo "Adjusting database name from ${DB_NAME_IN_BACKUP} to ${DB_NAME} in ${DB_BACKUP_FILE}"
        sed -i "s/${DB_NAME_IN_BACKUP}/${DB_NAME}/g" "${DB_BACKUP_FILE}"
    fi

    psql --host="$DB_HOST" --port="$DB_PORT" --username="$DB_USER" --no-password -c "CREATE DATABASE ${DB_NAME};"
    psql --dbname="${DB_NAME}" --file="${DB_BACKUP_FILE}" --host="$DB_HOST" --no-password --port="$DB_PORT" --username="$DB_USER"
    rm -f ${DB_BACKUP_FILE}

    #
    # Re-init trac installation
    #
    cd /srv/trac/data

    #rm -f -- conf/trac.ini
    #python3 -m 'template_substitute' _SERVERNAME DB_HOST DB_NAME \
    #    DB_PORT DB_USER PGPASSWORD SMTP_HOST SMTP_PASSWORD SMTP_PORT SMTP_USER \
    #    --input '/etc/trac.ini.template' --output 'conf/trac.ini'

    #Update the restore trac.ini
    update_key_value "link" "${_SERVERNAME}" "/srv/trac/data/conf/trac.ini"
    update_key_value "url" "${_SERVERNAME}" "/srv/trac/data/conf/trac.ini"
    update_key_value "base_url" "${_SERVERNAME}" "/srv/trac/data/conf/trac.ini"
    update_key_value "database" "postgres://${DB_USER}:${PGPASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}" "/srv/trac/data/conf/trac.ini"
    update_key_value "smtp_server" "${SMTP_HOST}" "/srv/trac/data/conf/trac.ini"
    update_key_value "smtp_password" "${SMTP_PASSWORD}" "/srv/trac/data/conf/trac.ini"
    update_key_value "smtp_port" "${SMTP_PORT}" "/srv/trac/data/conf/trac.ini"
    update_key_value "smtp_user" "${SMTP_USER}" "/srv/trac/data/conf/trac.ini"
    update_key_value "log_level" "INFO" "/srv/trac/data/conf/trac.ini"
    update_key_value "log_type" "stderr" "/srv/trac/data/conf/trac.ini"

    chown -R apache:www-data -- .
    unset PGPASSWORD
    unset SMTP_PASSWORD

    trac-admin . upgrade
    trac-admin . wiki upgrade

    # Overwrite WSGI wrapper with newly generated one.
    rm -fr -- wsgi/
    trac-admin . deploy wsgi

    if [ "${SVN_INTEGRATED}" == 1 ]; then
        # Resync Trac with the Subversion repository.
        echo "Resynchronizing svn repository"
        trac-admin . repository resync '*'
    else
        echo "SVN not integrated"
    fi
}

function update_key_value {
    echo "update_key_value key=$1 in file=$3"
    #TODO: check input arguments
    KEY=$1
    NEW_VALUE=$(echo ${2} | sed 's/\//\\\//g')
    FILE=$3

    #TODO: make whitespace around = flexible. also support key=value, key =value and key= value
    set +e
    CURRENT_LINE=$(grep "${KEY} = " "$FILE")
    set -e
    if [ "${CURRENT_LINE}" != "" ]; then
        CURRENT_VALUE=$(echo "${CURRENT_LINE}" | cut -d "=" -f 2 | xargs)
        #echo "Key=${KEY} found with value=${CURRENT_VALUE}"
        CURRENT_LINE_SED=$(echo ${CURRENT_LINE} | sed 's/\//\\\//g')
        sed -i "s/${CURRENT_LINE_SED}/${KEY} = ${NEW_VALUE}/g" "$FILE"
    else
        echo "Key=${KEY} not found in $FILE"
    fi
}

function main {
    #
    # Support environment variables:
    #   MODE        DO_SVN or DO_TRAC   required
    #   INIT        1 or 0              optional, 0 by default
    #   SLEEP       number              optional, 5 by default
    #   INIT_FILE   string              optional, /var/www/dynamic_cfg/initialised by default
    #

    #
    # Check required parameters
    #
    if [[ "${MODE}" != "DO_SVN" && "${MODE}" != "DO_TRAC" ]]; then
        echo "ERROR: Invalid mode specified: ${MODE}"
        echo "       Valid modes: DO_SVN, DO_TRAC"
        exit 1
    fi

    #
    # Restore backup if needed
    #
    if [ -e /backups/restore ]; then
        echo "Backup found, restoring"
        wait_for_database
        restore
        touch /init/exit
        exit 0
    fi

    #
    # Initialize if needed
    #
    if [ ${_INIT} -eq 1 ]; then
        echo "Initializing"
        wait_for_database
        initialize
        touch /init/exit
        exit 0
    fi

    #
    # Continue to run normally
    #
    wait_for_database
    wait_for_preparer
}

main "$@"; exit