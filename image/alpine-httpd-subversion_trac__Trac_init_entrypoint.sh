#!/bin/sh

set -ex

#
#Store database connection details
#
echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USER}:${PGPASSWORD}" > ~/.pgpass
chmod 0600 ~/.pgpass

#
#Check and stop if there is an initialised trac instance
#
TRAC_INITIALISED=$(grep "postgres://${DB_USER}:${PGPASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}" conf/trac.ini | wc -l)
TRAC_DB_INITIALISED=$(psql --host="${DB_HOST}" --username="${DB_USER}" --no-password --port="${DB_PORT}" -tAc "SELECT 1 FROM pg_database WHERE datname='${DB_NAME}'")
if [ "${TRAC_INITIALISED}" == "1" ]; then
    printf '%s\n' 'Trac has already been initialised. '
    exit 1
fi

#
#Initialise a new trace instance
#
printf '%s\n' 'No trac installation found, initialising fresh environment.'

mkdir data
cd data

echo "Creating database: ${DB_NAME}"
psql --host="$DB_HOST" --port="$DB_PORT" --username="$DB_USER" --no-password -c "CREATE DATABASE ${DB_NAME};"
if [ -d "/srv/subversion" ]; then
    echo "Subversion repository detected"
    trac-admin . initenv test \
        "postgres://$DB_USER:$PGPASSWORD@$DB_HOST:$DB_PORT/$DB_NAME" \
         svn '/srv/subversion/' ;
    trac-admin . repository add "svn" "/srv/subversion"
else
    echo "No subversion repository detected"
    trac-admin . initenv test \
        "postgres://$DB_USER:$PGPASSWORD@$DB_HOST:$DB_PORT/$DB_NAME";
fi

rm -f -- conf/trac.ini
python3 -m 'template_substitute' _SERVERNAME DB_HOST DB_NAME \
    DB_PORT DB_USER PGPASSWORD SMTP_HOST SMTP_PASSWORD SMTP_PORT SMTP_USER \
    --input '/etc/trac.ini.template' --output 'conf/trac.ini'
chown -R apache:www-data -- .
unset PGPASSWORD
# Overwrite WSGI wrapper with newly generated one.
rm -fr -- wsgi/
trac-admin . deploy wsgi/

# Resync Trac with the Subversion repository.
echo "Resyncing all svn repositories"
trac-admin . repository resync '*'