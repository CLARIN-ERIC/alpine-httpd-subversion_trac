#!/bin/sh

set -e
if [ "$1" = "PUBLIC_READ_ACCESS" ]; then
    echo "Public svn auth"
    python3 -m 'template_substitute' AUTH_LDAP_BIND_DN \
        AUTH_LDAP_BIND_PASSWORD AUTH_LDAP_REQUIRE AUTH_LDAP_URL \
        --input '/etc/apache2/conf.d/auth.conf.public.template' --output \
        '/var/www/dynamic_cfg/auth.conf'
elif [ "$1" = "BASIC_AUTH" ]; then
    echo "Basic auth"
    cp '/etc/apache2/conf.d/auth.conf.basic.template' '/var/www/dynamic_cfg/auth.conf'
else
    echo "Restricted auth"
    python3 -m 'template_substitute' AUTH_LDAP_BIND_DN \
        AUTH_LDAP_BIND_PASSWORD AUTH_LDAP_REQUIRE AUTH_LDAP_URL \
        --input '/etc/apache2/conf.d/auth.conf.template' --output \
        '/var/www/dynamic_cfg/auth.conf'
fi